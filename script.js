/* global Vue */

// TODO: use a SAT-solver to tell the user whether they must guess.

// TODO: replace showLoss() and showWin() with something less annoying.

// TODO: document.addEventListener('error', evt => show_warning)
//       - maybe extract into a library and <script src> it on all my projects

function countif(arr, predicate) {
  // replacement for arr.filter(predicate).length;
  let count = 0;
  for (let elem of arr) {
    if (predicate(elem)) { count++; }
  }
  return count;
}
function sample(population, size) {
  let pool = population.slice();
  let result = new Array(size);
  for (let i=0; i<size; i++) {
    const j = Math.random() * (population.length - 1) | 0;
    result[i] = pool[j];
    pool[j] = pool[population.length - i - 1];
  }
  return result;
}

Vue.component('minefield', {
  data: function() {
    const [num_cols, num_rows] = [30, 16];
    return {
      // auto-solver options:
      autosolver_zero: false,
      autosolver_enough_flags: false,
      autosolver_enough_open: false,
      // display options:
      cell_px: 20, solver_delay_ms: 50,
      // game options:
      num_prerevealed: 7,
      num_cols, num_rows,
      num_bombs: Math.floor(num_cols * num_rows * .21),
      // game board state:
      cells: [],
      neighbors_of: new Map(),
      // game situation:
      num_losses: 0,
      seconds: 0,
      won: false,
      // timers:
      updateTimer_interval_id: null,
      solveAll_timeout_id: null,
    };
  },
  template: '#minefield-template',
  created: function() { this.reset(); },
  beforeDestroy: function() {
    clearInterval(this.updateTimer_interval_id);
    clearTimeout(this.solveAll_timeout_id);
  },
  computed: {
    width: function() { return this.cell_px * this.num_cols; },
    height: function() { return this.cell_px * this.num_rows; },
  },
  methods: {
    onLeftClick: function(idx) {
      const cell = this.cells[idx];
      if (cell === undefined) { console.error(idx); }
      if (!cell.is_open && !cell.is_flagged) {
        if (cell.is_bomb) {
          this.showLoss();
        } else {
          Vue.set(cell, 'is_open', true);
        }
      }
      this.solveAll();
      this.checkForFinish();
    },
    onRightClick: function(idx) {
      const cell = this.cells[idx];
      if (!cell.is_open) {
        if (!cell.is_flagged && !cell.is_bomb) {
          this.showLoss();
        } else {
          Vue.set(cell, 'is_flagged', !cell.is_flagged);
        }
      }
      this.solveAll();
      this.checkForFinish();
    },
    onMiddleClick: function(idx) {
      const cell = this.cells[idx];
      const neighbors = this.neighbors_of.get(idx).map(i => this.cells[i]);
      const num_neighboring_flags = countif(neighbors, c => c.is_flagged);
      const num_neighboring_closed = countif(neighbors, c => !c.is_open);
      const closed_unflagged_neighbors = neighbors.filter(c => !c.is_open && !c.is_flagged);
      if (cell.num_neighboring_bombs === num_neighboring_flags) {
        // If neighboring flags account for all the mines, open the rest.
        if (closed_unflagged_neighbors.some(c => c.is_bomb)) {
          this.showLoss();
        } else {
          closed_unflagged_neighbors.forEach(c => Vue.set(c, 'is_open', true));
        }
      } else if (cell.num_neighboring_bombs === num_neighboring_closed) {
        // If all remaining hidden neighbors must be bombs, flag them.
        if (closed_unflagged_neighbors.some(c => !c.is_bomb)) {
          this.showLoss();
        } else {
          closed_unflagged_neighbors.forEach(c => Vue.set(c, 'is_flagged', true));
        }
      }
      this.solveAll();
      this.checkForFinish();
    },
    solveAll: function() {
      clearTimeout(this.solveAll_timeout_id);
      this.solveAll_timeout_id = null;
      let changes_to_make = [];
      for (const cell of this.cells.filter(c => c.is_open)) {
        const neighbors = this.neighbors_of.get(cell.idx).map(idx => this.cells[idx]);
        const num_neighboring_flags = countif(neighbors, c => c.is_flagged);
        const num_neighboring_closed = countif(neighbors, c => !c.is_open);
        const closed_unflagged_neighbors = neighbors.filter(c => !c.is_open && !c.is_flagged);
        if (cell.num_neighboring_bombs < num_neighboring_flags) {
          return this.showLoss();
        } else if (cell.num_neighboring_bombs === 0 && this.autosolver_zero || cell.num_neighboring_bombs === num_neighboring_flags && this.autosolver_enough_flags) {
          // If neighboring flags account for all the mines, open the rest.
          closed_unflagged_neighbors.forEach(c => { changes_to_make.push({idx: c.idx, param_to_set: 'is_open'}); });
        } else if (cell.num_neighboring_bombs === num_neighboring_closed && this.autosolver_enough_open) {
          // If all remaining hidden neighbors must be bombs, flag them.
          closed_unflagged_neighbors.forEach(c => { changes_to_make.push({idx: c.idx, param_to_set: 'is_flagged'}); });
        }
      }
      if (changes_to_make.length) {
        if (changes_to_make.some(o => o.param_to_set === 'is_flagged' && !this.cells[o.idx].is_bomb || o.param_to_set==='is_open' && this.cells[o.idx].is_bomb)) {
          this.showLoss();
        } else {
          for (let {param_to_set, idx} of changes_to_make) {
            Vue.set(this.cells[idx], param_to_set, true);
          }
          if (!this.checkForFinish()) {
            this.solveAll_timeout_id = setTimeout(this.solveAll, this.solver_delay_ms);
          }
        }
      }
    },
    checkForFinish: function() {
      if (this.cells.some(cell => cell.is_bomb && cell.is_open)) {
        this.showLoss();
        return 'loss';
      } else if (this.cells.every(cell => cell.is_flagged === cell.is_bomb && cell.is_bomb === !cell.is_open)) {
        this.showWin();
        return 'win';
      }
    },
    showWin: function() {
      if (!this.won) {
        clearInterval(this.updateTimer_interval_id);
        this.updateTimer_interval_id = null;
        setTimeout(() => alert((this.num_losses===0) ? 'won' : 'finished'), 10);
      }
      this.won = true;
    },
    showLoss: function() { this.num_losses++; setTimeout(() => alert('loss'), 10); },
    reset: function() {
      const num_cells = this.num_cols * this.num_rows;

      this.neighbors_of.clear();
      for (const idx of Array(num_cells).keys()) {
        const [row, col] = [Math.floor(idx / this.num_cols), idx % this.num_cols];
        const neighbor_offsets = [[-1,-1],[-1,0],[-1,1],[0,-1],[0,1],[1,-1],[1,0],[1,1]];
        this.neighbors_of.set(
          idx,
          neighbor_offsets
            .map(([rowdelta, coldelta]) => [row+rowdelta, col+coldelta])
            .filter(([r, c]) => 0<=r && r<this.num_rows && 0<=c && c<this.num_cols)
            .map(([r, c]) => r*this.num_cols + c)
        );
      }

      // Note: Vue is observing `this`, so it notices us setting `this.cells`.
      //       It checks each element of the new array and adds getter and setter methods
      //       for each key of each object.
      //       Unfortunately Vue can't notice any keys added after this assignment unless we use `Vue.set(obj,key,val)`.
      this.cells = [...Array(num_cells).keys()].map(idx => ({
        idx: idx,
        is_open: false,
        is_bomb: false,
        is_flagged: false,
        num_neighboring_bombs: 0,
      }));
      for (const cell of sample(this.cells, this.num_bombs)) { Vue.set(cell, 'is_bomb', true); }
      for (const cell of this.cells) { Vue.set(cell, 'num_neighboring_bombs', countif(this.neighbors_of.get(cell.idx), idx => this.cells[idx].is_bomb)); }
      for (const cell of this.cells.slice(0, this.num_prerevealed)) { Vue.set(cell, (cell.is_bomb ? 'is_flagged' : 'is_open'), true); }

      Vue.set(this, 'num_losses', 0);
      Vue.set(this, 'seconds', 0);
      Vue.set(this, 'won', false);
      clearInterval(this.updateTimer_interval_id);
      this.updateTimer_interval_id = setInterval(this.updateTimer, 1000);
      this.solveAll();
    },
    updateTimer: function() { this.seconds++; },
  },
  components: {
    'square': {
      props: {
        cell:Object, col:Number, row:Number, cell_px:Number,
      },
      template: '#square-template',
      computed: {
        transform: function() {
          return `translate(${this.col*this.cell_px},${this.row*this.cell_px})`;
        },
        bgColor: function() {
          return this.cell.is_flagged ? 'red' :
            this.cell.is_open ? 'white' : 'black';
        },
        textColor: function() {
          return this.cell.is_flagged ? 'red' : 'black';
        }
      },
    }
  }
});

const sweep = new Vue({
  el: '#sweep',
});

/* exported minefield */
const minefield = sweep.$children[0];
